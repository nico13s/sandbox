#/bin/bash

./bin/console doctrine:database:create
./bin/console doctrine:schema:update --force
./bin/console assets:install --symlink
./bin/console fos:user:create admin admin@yopmail.com 123456

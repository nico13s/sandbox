<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Nico13s\WebBundle\WebBundle(),
//            new Nico13s\AdminBundle\AdminBundle(),
            new Nico13s\CoreBundle\CoreBundle(),
            new FOS\UserBundle\FOSUserBundle(),
//            new JavierEguiluz\Bundle\EasyAdminBundle\EasyAdminBundle(),
//            new Nelmio\ApiDocBundle\NelmioApiDocBundle(),
            new ApiPlatform\Core\Bridge\Symfony\Bundle\ApiPlatformBundle(),
//            new Lexik\Bundle\JWTAuthenticationBundle\LexikJWTAuthenticationBundle(),
            new Misd\PhoneNumberBundle\MisdPhoneNumberBundle(),
            new Apoutchika\LoremIpsumBundle\ApoutchikaLoremIpsumBundle(),
            new Debril\RssAtomBundle\DebrilRssAtomBundle(),
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
            $bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function getRootDir()
    {
        return __DIR__;
    }

    public function getCacheDir()
    {
//        return dirname(__DIR__).'/var/cache/'.$this->getEnvironment();
        return $this->getTmpFolder().'/cache/'.$this->getEnvironment();
    }

    public function getLogDir()
    {
//        return dirname(__DIR__).'/var/logs';
        return $this->getTmpFolder().'/logs';
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }

    protected function getTmpFolder(){
        return '/tmp/sandbox/';
    }
}

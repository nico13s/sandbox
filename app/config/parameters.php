<?php
    $container->setParameter('mailer_transport', '%env(SB_SW_MAILER_TRANSPORT)%');
    $container->setParameter('env(SB_SW_MAILER_TRANSPORT)', 'smtp');

    $container->setParameter('mailer_host', '%env(SB_SW_MAILER_HOST)%');
    $container->setParameter('env(SB_SW_MAILER_HOST)', 'localhost');

    $container->setParameter('mailer_user', '%env(SB_SW_MAILER_USER)%');
    $container->setParameter('env(SB_SW_MAILER_USER)', '~');

    $container->setParameter('mailer_password', '%env(SB_SW_MAILER_PASSWORD)%');
    $container->setParameter('env(SB_SW_MAILER_PASSWORD)', '~');


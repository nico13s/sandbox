<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Debug\Debug;

/** @var \Composer\Autoload\ClassLoader $loader */
$loader = require __DIR__.'/../app/autoload.php';
    $sfEnv = 'prod';
    $sfDebug = false;
    $HTTP_SF_HEADER_ENV = getenv('HTTP_SF_HEADER_ENV');
    if(true === in_array($HTTP_SF_HEADER_ENV, array('dev', 'test'))){
        $sfDebug = true;
        Debug::enable();
//        BenchConsole::start();

        $UA = getenv('HTTP_USER_AGENT');
        if(strtolower($UA) == 'behat'){
            $sfEnv = 'test';
        }

    } else {
        include_once __DIR__.'/../var/bootstrap.php.cache';
    }

$kernel = new AppKernel($sfEnv, $sfDebug);
$kernel->loadClassCache();

$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);

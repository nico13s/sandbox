# Install

## Lancement du projet

```shell
git clone git@gitlab.com:nico13s/sandbox.git
composer install
bin/console doctrine:database:create
bin/console doctrine:schema:update --force
```

## Installation des assets

```shell
bin/console assets:install --symlink
```

#1 Création d'un utilisateur

```shell
bin/console fos:user:create admin admin@yopmail.com 123456
```
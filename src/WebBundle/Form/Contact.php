<?php

namespace Nico13s\WebBundle\Form;

use Symfony\Component\Validator\Constraints as Assert;

class Contact {

    // <editor-fold defaultstate="collapsed" desc="Members">
    /**
     * @var String $name
     * @Assert\NotBlank()
     */
    protected $name;

    /**
     * @var String $email
     */
    protected $email;

    /**
     * @var String $phone
     */
    protected $phone;

    /**
     * @var String $message
     */
    protected $message;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Getters">
    /**
     * @return String
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return String
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return String
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return String
     */
    public function getMessage()
    {
        return $this->message;
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Setters">
    /**
     * @param String $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param String $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param String $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @param String $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }
    // </editor-fold>
}

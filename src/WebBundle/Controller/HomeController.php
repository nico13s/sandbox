<?php

namespace Nico13s\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller
{
    public function indexAction()
    {
        return $this->render('WebBundle:Home:index.html.twig');
    }

}

<?php

namespace Nico13s\WebBundle\Controller;

use Nico13s\WebBundle\Form\Contact;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;

class ContactController extends Controller
{

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {

        $oContactForm = new Contact();
        $oForm = $this->createFormBuilder($oContactForm)
            ->setAction($this->generateUrl('web_contact'))
            ->setMethod('POST')
            ->add('email', EmailType::class, array(
                'attr' => array(
                    'id' => 'email',
                    'class' => 'form-control',
                    'placeholder' => 'Email Address',
                    'data-validation-required-message' => 'Please enter your email address.',
                ),
                'required' => true
            ))
            ->add('message', TextareaType::class, array(
                'attr' => array(
                    'id' => 'message',
                    'class' => 'form-control',
                    'placeholder' => 'Message',
                    'data-validation-required-message' => 'Please enter your message.',
                ),
                'required' => true
            ))
            ->add('name', TextType::class, array(
                'attr' => array(
                    'id' => 'name',
                    'class' => 'form-control',
                    'placeholder' => 'Name',
                    'data-validation-required-message' => 'Please enter your name.',
                ),
                'required' => true
            ))
            ->add('phone', TextType::class, array(
                'attr' => array(
                    'id' => 'phone',
                    'class' => 'form-control',
                    'placeholder' => 'Phone Number',
//                    'data-validation-required-message' => 'Please enter your phone number.',
                ),
                'required' => false
            ))
//            ->add('dueDate', DateType::class)
            ->add('save', SubmitType::class, array(
                'attr' => array(
                    'class' => 'btn btn-default',
                ),
                'label' => 'Send'
            ))
            ->getForm();

        $oForm->handleRequest($request);

        if ($oForm->isSubmitted() && $oForm->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            /**
             * @var Contact $oContactForm
             */
            $oContactForm = $oForm->getData();
//            echo '<pre>';var_dump(array(
//                $oContactForm,
//                get_class_methods($oContactForm),
//                $this->getParameter('mailer_user')
//            ));die;

            $message = \Swift_Message::newInstance()
                ->setSubject('Contact Sandbox de'.$oContactForm->getEmail())
                ->setFrom($oContactForm->getEmail())
                ->setTo('nicolas.sias@gmail.com')
                ->setBody(
                    $oContactForm->getMessage(),
//                    $this->renderView(
//                    // app/Resources/views/Emails/registration.html.twig
//                        'Emails/registration.html.twig',
//                        array('name' => $name)
//                    ),
                    'text/html'
                )
                /*
                 * If you also want to include a plaintext version of the message
                ->addPart(
                    $this->renderView(
                        'Emails/registration.txt.twig',
                        array('name' => $name)
                    ),
                    'text/plain'
                )
                */
            ;
            $this->get('mailer')->send($message);
        }
        return $this->render('WebBundle:Contact:index.html.twig', array(
            'form' => $oForm->createView(),
        ));
    }
}

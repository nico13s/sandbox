<?php

namespace Nico13s\CoreBundle\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * A Feed - this description will be automatically extracted form the PHPDoc to document the API.
 *
 * @ORM\HasLifecycleCallbacks
 * @ApiResource(iri="http://schema.org/DataFeed")
 * @ORM\Table(name="feed")
 * @ORM\Entity
 * @UniqueEntity("url")
 */
class Feed
{
    // <editor-fold defaultstate="collapsed" desc="Members">
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer $id
     */
    protected $id;

    /**
     * @param string $name - Feed Name
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=50)
     * @Assert\NotBlank
     */
    protected $name;

    /**
     * @param string $url - Feed url
     * @var string $url
     *
     * @ORM\Column(name="url", type="string", length=255)
     * @Assert\NotBlank
     */
    protected $url;

    /**
     * @param string dateCreated
     * @ORM\Column(name="created", type="datetime")
     * @var DateTime
     */
    protected $created;

    /**
     * @ORM\Column(name="updated", type="datetime")
     * @var DateTime
     */
    protected $updated;

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Getters">
    /**
     * @return Integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return String
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @return String
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * @return DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * @return DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Setters">

    /**
     * @param mixed $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url) {
        $this->url = $url;
    }

    /**
     * @param DateTime $created
     */
    public function setCreated($created) {
        $this->created = $created;
    }

    /**
     * @param DateTime $updated
     */
    public function setUpdated($updated) {
        $this->updated = $updated;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Runners">
    /**
     * Fonction appelée automatique avant persist
     *
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        if(null === $this->getCreated()){
            $this->setCreated(new \DateTime());
        }
        $this->setUpdated(new \DateTime());
    }
    // </editor-fold>

}
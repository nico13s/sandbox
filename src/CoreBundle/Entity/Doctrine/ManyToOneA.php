<?php

namespace Nico13s\CoreBundle\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Exemple relation Many To One - Entity A
 *
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="doctrine_manytoone_a")
 * @ORM\Entity()
 */
class ManyToOneA
{
    // <editor-fold defaultstate="collapsed" desc="Members">
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=50)
     */
    protected $name;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Getters">
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Setters">
    /**
     * @param mixed $name
     */
    public function setName($name) {
        $this->name = $name;
    }
    // </editor-fold>

}


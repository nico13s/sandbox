<?php

namespace Nico13s\CoreBundle\Entity\Doctrine;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Exemple relation Many To Many - Entity B
 *
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="doctrine_manytomany_b")
 * @ORM\Entity()
 */
class ManyToManyB
{
    // <editor-fold defaultstate="collapsed" desc="Members">
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=50)
     */
    protected $name;

    /**
     * @ORM\ManyToMany(targetEntity="Nico13s\CoreBundle\Entity\Doctrine\ManyToManyA", cascade={"persist", "merge"}, mappedBy="manytomanybs")
     * @var ArrayCollection
     */
    private $manytomanyas;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Magic Methods">
    /**
     * ManyToMany_B constructor.
     *
     */
    public function __construct() {
        $this->manytomanyas = new ArrayCollection();
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Getters">
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @return ArrayCollection
     */
    public function getManytomanyas() {
        return $this->manytomanyas;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Setters">
    /**
     * @param ManyToManyA $manyToManyA
     */
    public function addManyTomanyA(ManyToManyA $manyToManyA){
        if(false === $this->manytomanyas->contains($manyToManyA)){
            if(false === $manyToManyA->getManytomanybs()->contains($this)){
                $manyToManyA->addManyTomanyB($this);
            }
            $this->manytomanyas->add($manyToManyA);
        }
    }

    /**
     * @param ManyToManyA $manyToManyA
     */
    public function removeManyTomanyA(ManyToManyA $manyToManyA){
        $this->manytomanyas->removeElement($manyToManyA);
    }

    /**
     * @param mixed $name
     */
    public function setName($name) {
        $this->name = $name;
    }
    // </editor-fold>

}


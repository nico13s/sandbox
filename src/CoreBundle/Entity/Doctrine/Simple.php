<?php

namespace Nico13s\CoreBundle\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Simple
 *
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="doctrine_simple")
 * @ORM\Entity(repositoryClass="Nico13s\CoreBundle\Repository\Doctrine\SimpleRepository")
 */
class Simple
{
    // <editor-fold defaultstate="collapsed" desc="Members">
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=50)
     * @Assert\Length(
     *     min = 0,
     *     max = 50,
     *     exactMessage = "Name has to have {{ limit }} characters max."
     * )
     */
    protected $name;

    /**
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     * @Assert\Length(
     *     min = 0,
     *     max = 255,
     *     exactMessage = "Description has to have {{ limit }} characters max."
     * )
     */
    protected $description;

    /**
     * @ORM\Column(name="created", type="datetime")
     * @var DateTime
     */
    protected $created;

    /**
     * @ORM\Column(name="updated", type="datetime")
     * @var DateTime
     */
    protected $updated;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Getters">
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * @return mixed
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @return DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Setters">
    /**
     * @param DateTime $created
     */
    public function setCreated($created) {
        $this->created = $created;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * @param mixed $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @param DateTime $updated
     */
    public function setUpdated($updated) {
        $this->updated = $updated;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Runners">
    /**
     * Fonction appelée automatique avant persist
     *
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        if(null === $this->getCreated()){
            $this->setCreated(new \DateTime());
        }
        $this->setUpdated(new \DateTime());
    }
    // </editor-fold>

}


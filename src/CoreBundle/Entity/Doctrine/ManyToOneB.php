<?php

namespace Nico13s\CoreBundle\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Exemple relation Many To One - Entity B - Propriétaire de la relation
 *
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="doctrine_manytoone_b")
 * @ORM\Entity()
 */
class ManyToOneB
{
    //<editor-fold defaultstate="collapsed" desc="Members">
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="description", type="text")
     * @var String
     */
    protected $description;

    /**
     * @ORM\ManyToOne(targetEntity="Nico13s\CoreBundle\Entity\Doctrine\ManyToOneA", cascade={"persist"})
     * @ORM\JoinColumn(name="a_id", referencedColumnName="id")
     * @var ManyToOneA;
     */
    private $ManyToOneA;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Getters">
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @return ManyToOne_A
     */
    public function getManyToOneA() {
        return $this->ManyToOneA;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Setters">
    /**
     * @param mixed $description
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * @param ManyToOneA $ManyToOneA
     */
    public function setManyToOneA($ManyToOneA) {
        $this->ManyToOneA = $ManyToOneA;
    }
    // </editor-fold>

}


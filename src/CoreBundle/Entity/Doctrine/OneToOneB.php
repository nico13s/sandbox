<?php

namespace Nico13s\CoreBundle\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Exemple relation One To One - Entity B - Propriétaire de la relation
 *
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="doctrine_onetoone_b")
 * @ORM\Entity()
 */
class OneToOneB
{
    //     <editor-fold defaultstate="collapsed" desc="Members">
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="description", type="text")
     * @var String
     */
    protected $description;

    /**
     * @ORM\OneToOne(targetEntity="Nico13s\CoreBundle\Entity\Doctrine\OneToOneA", cascade={"persist"})
     * @ORM\JoinColumn(name="a_id", referencedColumnName="id", nullable=true)
     * @var OneToOneA;
     */
    private $oneToOneA;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Getters">
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @return OneToOneA
     */
    public function getOneToOneA() {
        return $this->oneToOneA;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Setters">
    /**
     * @param mixed $description
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * @param OneToOne_A $oneToOneA
     */
    public function setOneToOneA(OneToOneA $oneToOneA) {
        $this->oneToOneA = $oneToOneA;
    }
    // </editor-fold>

}


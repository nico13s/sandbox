<?php

namespace Nico13s\CoreBundle\Entity\Doctrine;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Exemple relation Many To Many - Entity A
 *
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="doctrine_manytomany_a")
 * @ORM\Entity()
 */
class ManyToManyA
{
    // <editor-fold defaultstate="collapsed" desc="Members">
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=50)
     */
    protected $name;

    /**
     * @ORM\ManyToMany(targetEntity="Nico13s\CoreBundle\Entity\Doctrine\ManyToManyB", cascade={"persist", "merge"}, inversedBy="manytomanyas")
     * @ORM\JoinTable( name="many_to_many_a_many_to_many_b",
     *  joinColumns={@ORM\JoinColumn(name="many_to_many_a_id", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="many_to_many_b_id", referencedColumnName="id")}
     * )
     * @var ArrayCollection
     */
    private $manytomanybs;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Magic Methods">
    /**
     * ManyToManyA constructor.
     *
     */
    public function __construct() {
        $this->manytomanybs = new ArrayCollection();
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Getters">
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @return ArrayCollection
     */
    public function getManytomanybs() {
        return $this->manytomanybs;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Setters">
    /**
     * @param ManyToManyB $manyToManyB
     */
    public function addManyTomanyB(ManyToManyB $manyToManyB){
        $this->manytomanybs[] = $manyToManyB;
    }

    /**
     * @param ManyToManyB $manyToManyB
     */
    public function removeManyTomanyB(ManyToManyB $manyToManyB){
        $this->manytomanybs->removeElement($manyToManyB);
    }

    /**
     * @param mixed $name
     */
    public function setName($name) {
        $this->name = $name;
    }
    // </editor-fold>

}

<?php

    namespace Nico13s\CoreBundle\Command;

    use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
    use Symfony\Component\Console\Input\ArrayInput;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Output\ConsoleOutput;
    use Symfony\Component\Console\Output\OutputInterface;

    /**
     * Class BuildEnvironmentCommand
     * @package Nico13s\CoreBundle\Command
     */
    class BuildEnvironmentCommand extends ContainerAwareCommand {

        /**
         *
         */
        protected function configure()
        {
            $this
                // the name of the command (the part after "bin/console")
                ->setName('sandbox:build')

                // the short description shown while running "php bin/console list"
                ->setDescription('Launch this script to build environment')
                // the full command description shown when running the command with
                // the "--help" option
                ->setHelp('This text is displayed if user need help...')
            ;
        }

        /**
         * @param InputInterface  $input
         * @param OutputInterface $output
         *
         * @return bool
         */
        protected function execute(InputInterface $input, OutputInterface $output)
        {
            $output->writeln('Command start');
            $this->buildDatabase();
            $this->updateDatabase();
            $this->launchFixtures();
            $output->writeln('Command end');
            return true;
        }

        /**
         * @throws \Exception
         */
        protected function buildDatabase(){
            $application = $this->getApplication();
            $application->setAutoExit(false);
            $input = new ArrayInput(array(
                'command' => 'doctrine:database:create',
                '--env' => $this->getContainer()->getParameter('kernel.environment'),
                '--if-not-exists' => true
            ));
            $output = new ConsoleOutput();
            $application->run($input, $output);
        }

        /**
         * @throws \Exception
         */
        protected function launchFixtures(){
            $application = $this->getApplication();
            $application->setAutoExit(false);
            $input = new ArrayInput(array(
                'command' => 'doctrine:fixtures:load',
                '--env' => $this->getContainer()->getParameter('kernel.environment'),
                '-q' => true
            ));
            $output = new ConsoleOutput();
            $application->run($input, $output);
        }

        /**
         *
         * @throws \Exception
         */
        protected function updateDatabase(){
            $application = $this->getApplication();
            $application->setAutoExit(false);
            $input = new ArrayInput(array(
                'command' => 'doctrine:schema:update',
                '--env' => $this->getContainer()->getParameter('kernel.environment'),
                '--force' => true
            ));
            $output = new ConsoleOutput();
            $application->run($input, $output);
        }
    }

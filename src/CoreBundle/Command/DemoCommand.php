<?php

namespace Nico13s\CoreBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\InvalidOptionException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DemoCommand extends ContainerAwareCommand {

    /**
     *
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:demo')

            // the short description shown while running "php bin/console list"
            ->setDescription('Example of Command')

            ->addOption('option', 'o', InputOption::VALUE_OPTIONAL, 'Option provided - default false', false)
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This text is displayed if user need help...')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return bool
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Command start');
        try {
            $option = $input->getOption('option');
            if(false !== $option){
                $output->writeln('Option provided: '.$option);
            }
        } catch(InvalidOptionException $oIOE){
            throw $oIOE;
        }

        // green text
        $output->writeln('Text color <fg=green>foo</>');

        // black text on a cyan background
        $output->writeln('Text color background cyan and <fg=black;bg=cyan>black</>');

        // bold text on a yellow background
        $output->writeln('Text color background yellow and text <bg=yellow;options=bold>white and bold</>');

        // bold text with underscore
        $output->writeln('Text color bold and <options=bold,underscore>underlined</>');
        $output->writeln('Text verbosity debug', OutputInterface::VERBOSITY_DEBUG);
        $output->writeln('Command end');
        return true;
    }
}

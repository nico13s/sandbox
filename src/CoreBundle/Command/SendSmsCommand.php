<?php

namespace Nico13s\CoreBundle\Command;

use libphonenumber\RegionCode;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\InvalidOptionException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Ovh\Sms\SmsApi;

class SendSmsCommand extends ContainerAwareCommand {

    /**
     *
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('sandbox:sms:send')

            // the short description shown while running "php bin/console list"
            ->setDescription('Example to send an sms from ovh api')
            ->addOption('recipient', 'r', InputOption::VALUE_REQUIRED, 'Recipient Phone number ', false)
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This text is displayed if user need help...')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return bool
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Command start');

        $result = false;
        try {
            $recipientNumber = $input->getOption('recipient');
            if(false === $recipientNumber){
                $output->writeln('<fg=red>Please provide a phone number.</>');
            }
        } catch(InvalidOptionException $oIOE){
            throw $oIOE;
        }
        try {
            $oPhoneNumber = $this->getContainer()->get('libphonenumber.phone_number_util')->parse($recipientNumber, RegionCode::FR);
            $isFrenchPhoneNumber = $this->getContainer()->get('libphonenumber.phone_number_util')->isValidNumberForRegion($oPhoneNumber, RegionCode::FR);
            if(false === $isFrenchPhoneNumber){
                throw new \Exception('Le numéro fourni n\'est pas un numéro francais');
            }
        } catch(\Exception $e){
            $output->writeln('<fg=red>'.$e->getMessage().'</>');
            throw $e;
        }

        try {
            $applicationKey = $this->getContainer()->getParameter('ovh_sms_application_key');
            $applicationSecret = $this->getContainer()->getParameter('ovh_sms_application_secret');
            $consumerKey = $this->getContainer()->getParameter('ovh_sms_consumer_key');
            $endpoint = $this->getContainer()->getParameter('ovh_sms_endpoint');

            // Init SmsApi object
            $Sms = new SmsApi( $applicationKey, $applicationSecret, $endpoint, $consumerKey );

            $accounts = $Sms->getAccounts();
            // Set the account you will use
            $Sms->setAccount($accounts[0]);

            $senders = $Sms->getSenders();

            // Create a new message that will allow the recipient to answer (to FR receipients only)
//        $Message = $Sms->createMessage(true);
            $Message = $Sms->createMessage(false);
            $Message->setSender('Sandbox');
            $Message->addReceiver($this->getContainer()->get('libphonenumber.phone_number_util')->format($oPhoneNumber, RegionCode::FR));
            $Message->setIsMarketing(false);
            $result = $Message->send('Hello world at '.date('H:i:s').'!');
        } catch(\Exception $e){
        }

        if($result == true){
            $output->writeln('Sms <fg=green>sent</>');
        } else {
            $output->writeln('Sms <fg=red>failed</>');
        }

        $output->writeln('Command end');
        return true;
    }
}

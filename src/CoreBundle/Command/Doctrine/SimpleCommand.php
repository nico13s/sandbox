<?php

namespace Nico13s\CoreBundle\Command\Doctrine;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\InvalidOptionException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Nico13s\CoreBundle\Entity\Doctrine\Simple;

class SimpleCommand extends ContainerAwareCommand {

    /**
     *
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('sandbox:demo:doctrine:simple')

            // the short description shown while running "php bin/console list"
            ->setDescription('Example to send an sms from ovh api')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This text is displayed if user need help...')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return bool
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Command start');

        $oEM = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $oRepository = $oEM->getRepository('CoreBundle:Doctrine\Simple');
        $oSimple = $this->getOneEntity($oRepository);
        if(null === $oSimple){
            $output->writeln('<fg=red>No entity found.</>');
            return null;
        } else {
            $output->writeln('<fg=green>One entity found.</>');
        }
        try {
            $this->updateDescription($oEM, $oSimple);
        } catch(\Exception $e){

        }
        $output->writeln('<fg=green>Entity updated.</>');

        $output->writeln('Command end');
        return true;
    }

    /**
     * @param EntityRepository $oRepository
     *
     * @return null|object
     */
    protected function getOneEntity(EntityRepository $oRepository){
        return $oRepository->findOneBy(array(), array('id' => 'asc'));
    }

    /**
     * @param EntityManager $oEM
     * @param Simple        $oSimple
     *
     * @return Simple
     */
    protected function updateDescription(EntityManager $oEM, Simple $oSimple){
        $loremIpsum = $this->getContainer()->get('apoutchika.lorem_ipsum');
        $description = $loremIpsum->getWords(10);
        $oSimple->setDescription($description);
        $oEM->persist($oSimple);
        $oEM->flush();
        return $oSimple;
    }
}

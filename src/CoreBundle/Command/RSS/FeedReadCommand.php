<?php

namespace Nico13s\CoreBundle\Command\RSS;

use Doctrine\ORM\EntityManager;
use FeedIo\Feed;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\InvalidOptionException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FeedReadCommand extends ContainerAwareCommand {

    /**
     *
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('rss:feed:read')

            // the short description shown while running "php bin/console list"
            ->setDescription('Call and parse rss feed')

            ->addOption('url', 'u', InputOption::VALUE_OPTIONAL, 'Feed url to read', false)
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This text is displayed if user need help...')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return bool
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Command start');
        // get feedio
        $feedIo = $this->getContainer()->get('feedio');

        // this date is used to fetch only the latest items
        $modifiedSince = new \DateTime('-1day');

        // the feed you want to read
//        $url = 'https://www.mac4ever.com/rss/actu';
//        $url = 'http://www.mac4ever.com/syndication/news.rss';
        $url= 'http://www.presse-citron.net/feed';
        $url= 'https://hackaday.com/blog/feed/';


        // now fetch its (fresh) content
//        $feed = $feedIo->read($url, new \Acme\Entity\Feed, $modifiedSince)->getFeed();
        $feed = $feedIo->read($url, new Feed(), $modifiedSince)->getFeed();
//        print_r(get_class($feed));
        foreach ( $feed as $item ) {
            print_r(array(
                get_class($item),
                get_class_methods($item),
                $item->getTitle(),
                $item->getPublicId(),
                $item->getLink(),
                $item->getLastModified(),
                $item->getDescription(),
                $item->getCategories(),
                $item->getMedias(),
                $item->toArray()
            ));
            die;
            echo "item title : {$item->getTitle()} \n ";
            // getMedias() returns enclosures if any
            $medias = $item->getMedias();
        }
//        feed:
//        try {
//            $option = $input->getOption('option');
//            if(false !== $option){
//                $output->writeln('Option provided: '.$option);
//            }
//        } catch(InvalidOptionException $oIOE){
//            throw $oIOE;
//        }
//
//        // green text
//        $output->writeln('Text color <fg=green>foo</>');
//
//        // black text on a cyan background
//        $output->writeln('Text color background cyan and <fg=black;bg=cyan>black</>');
//
//        // bold text on a yellow background
//        $output->writeln('Text color background yellow and text <bg=yellow;options=bold>white and bold</>');
//
//        // bold text with underscore
//        $output->writeln('Text color bold and <options=bold,underscore>underlined</>');
//        $output->writeln('Text verbosity debug', OutputInterface::VERBOSITY_DEBUG);
        $output->writeln('Command end');
        return true;
    }
}

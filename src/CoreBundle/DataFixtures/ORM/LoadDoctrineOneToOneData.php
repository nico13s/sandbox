<?php

namespace Nico13s\CoreBundle\DataFixtures\ORM;

use Nico13s\CoreBundle\DataFixtures\Loader\AbstractDoctrineOneToOneLoader;
use Nico13s\CoreBundle\Entity\Doctrine\OneToOneA;
use Nico13s\CoreBundle\Entity\Doctrine\OneToOneB;

/**
 * Class LoadDoctrineOneToOneData
 * @package Nico13s\CoreBundle\DataFixtures\ORM
 */
class LoadDoctrineOneToOneData extends AbstractDoctrineOneToOneLoader
{
    /**
     * Method to generate automated values
     * @return array
     */
    public function getAutomatedValues(){
        $aData = array();
        $serviceLoremIpsum = $this->getContainer()->get('apoutchika.lorem_ipsum');

        for($i=0; $i < 10; $i++){
            $oOTOA = new OneToOneA();
            $name = $serviceLoremIpsum->getSentences(mt_rand(1,2));
            $name = substr($name, 0, 50);
            $oOTOA->setName($name);
            if(mt_rand(1,100) %2 === 0){
                $oOTOB = new OneToOneB();
                $description = $serviceLoremIpsum->getSentences(mt_rand(2,5));
                $oOTOB->setDescription($description);
                $oOTOB->setOneToOneA($oOTOA);
                $aData[] = $oOTOB;
            } else {
                $aData[] = $oOTOA;
            }
        }
        return $aData;
    }

    /**
     * Return a list of specific values.
     *
     * @return array
     */
    public function getSpecificValues()
    {
        return [];
    }

    /**
     * Return the list of all values.
     *
     * @return array
     */
    public function getValues()
    {
        return array_merge($this->getSpecificValues(), $this->getAutomatedValues());
    }
}
<?php

namespace Nico13s\CoreBundle\DataFixtures\ORM;

use Nico13s\CoreBundle\DataFixtures\Loader\AbstractDoctrineSimpleLoader;
use Nico13s\CoreBundle\Entity\Doctrine\Simple;

/**
 * Class LoadDoctrineSimpleData
 * @package Nico13s\CoreBundle\DataFixtures\ORM
 */
class LoadDoctrineSimpleData extends AbstractDoctrineSimpleLoader
{
    /**
     * Method to generate automated values
     * @return array
     */
    public function getAutomatedValues(){
        $aData = array();
        $serviceLoremIpsum = $this->getContainer()->get('apoutchika.lorem_ipsum');
        for($i=0; $i < 10; $i++){
            $oSimple = new Simple();
            $name = $serviceLoremIpsum->getSentences(mt_rand(1,2));
            $name = substr($name, 0, 50);
            $oSimple->setName($name);
            if(mt_rand(1,100) %2 === 0){
                $description = $serviceLoremIpsum->getSentences(mt_rand(2,5));
                $description = substr($description, 0, 255);
                $oSimple->setDescription($description);
            }
            $aData[] = $oSimple;
        }
        return $aData;
    }

    /**
     * Return a list of specific values.
     *
     * @return array
     */
    public function getSpecificValues()
    {
//        return [
//            [
//                'sender'           => 'corky-profile',
//                'recipient'        => 'leblanc-profile',
//                'created_at'       => new \DateTime('2030-12-27 15:47:00'),
//                'updated_at'       => new \DateTime('2030-12-27 15:47:00'),
//                'read_at'          => null,
//                'delivered_at'     => null,
//                'spam_score'       => 0,
//                'status'           => 0,
//                'sender_hidden'    => false,
//                'recipient_hidden' => false,
//                'type'             => 0,
//                'object_id'        => null,
//                'paid'             => 0,
//                'object_type'      => null,
//                'content'          => 'hey my name is corki',
//            ],
//        ];
        return [];
    }

    /**
     * Return the list of all values.
     *
     * @return array
     */
    public function getValues()
    {
        return array_merge($this->getSpecificValues(), $this->getAutomatedValues());
    }
}
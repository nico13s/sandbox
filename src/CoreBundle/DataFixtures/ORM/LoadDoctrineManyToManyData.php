<?php

namespace Nico13s\CoreBundle\DataFixtures\ORM;

use Nico13s\CoreBundle\DataFixtures\Loader\AbstractDoctrineManyToManyLoader;
use Nico13s\CoreBundle\Entity\Doctrine\ManyToManyA;
use Nico13s\CoreBundle\Entity\Doctrine\ManyToManyB;

/**
 * Class LoadDoctrineManyToManyData
 * @package Nico13s\CoreBundle\DataFixtures\ORM
 */
class LoadDoctrineManyToManyData extends AbstractDoctrineManyToManyLoader
{
    /**
     * Method to generate automated values
     * @return array
     */
    public function getAutomatedValues(){
        $aData = array();
        $serviceLoremIpsum = $this->getContainer()->get('apoutchika.lorem_ipsum');

        /**
         * Generate A with 0 to 10 B relations
         */
        for($i=0; $i < 10; $i++){
            $oMTMA = new ManyToManyA();
            $name = $serviceLoremIpsum->getSentences(mt_rand(1,2));
            $name = substr($name, 0, 50);
            $oMTMA->setName($name);
            $nbSubElement = mt_rand(0,10);
            for($j=0; $j < $nbSubElement; $j++){
                $oMTMB = new ManyToManyB();
                $name = $serviceLoremIpsum->getSentences(mt_rand(1,2));
                $name = substr($name, 0, 50);
                $oMTMB->setName($name);
                $oMTMA->addManyTomanyB($oMTMB);
            }
            $aData[] = $oMTMA;
        }

        /**
         * Generate B with 0 to 10 A relations
         */
        for($i=0; $i < 10; $i++){
            $oMTMB = new ManyToManyB();
            $name = $serviceLoremIpsum->getSentences(mt_rand(1,2));
            $name = substr($name, 0, 50);
            $oMTMB->setName($name);
            $nbSubElement = 5; //mt_rand(0,10);
            for($j=0; $j < $nbSubElement; $j++){
                $oMTMA = new ManyToManyA();
                $name = $serviceLoremIpsum->getSentences(mt_rand(1,2));
                $name = substr($name, 0, 50);
                $oMTMA->setName($name);
                $oMTMB->addManyTomanyA($oMTMA);
            }
            $aData[] = $oMTMB;
        }
        return $aData;
    }

    /**
     * Return a list of specific values.
     *
     * @return array
     */
    public function getSpecificValues()
    {
        return [];
    }

    /**
     * Return the list of all values.
     *
     * @return array
     */
    public function getValues()
    {
        return array_merge($this->getSpecificValues(), $this->getAutomatedValues());
    }
}

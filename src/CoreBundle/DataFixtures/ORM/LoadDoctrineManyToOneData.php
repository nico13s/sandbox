<?php

namespace Nico13s\CoreBundle\DataFixtures\ORM;

use Nico13s\CoreBundle\DataFixtures\Loader\AbstractDoctrineManyToOneLoader;
use Nico13s\CoreBundle\Entity\Doctrine\ManyToOneA;
use Nico13s\CoreBundle\Entity\Doctrine\ManyToOneB;

/**
 * Class LoadDoctrineManyToOneData
 * @package Nico13s\CoreBundle\DataFixtures\ORM
 */
class LoadDoctrineManyToOneData extends AbstractDoctrineManyToOneLoader
{
    /**
     * Method to generate automated values
     * @return array
     */
    public function getAutomatedValues(){
        $aData = array();
        $serviceLoremIpsum = $this->getContainer()->get('apoutchika.lorem_ipsum');

        for($i=0; $i < 10; $i++){
            $oMTOA = new ManyToOneA();
            $name = $serviceLoremIpsum->getSentences(mt_rand(1,2));
            $name = substr($name, 0, 50);
            $oMTOA->setName($name);
            if(mt_rand(1,5) > 4){
                $oMTOB = new ManyToOneB();
                $description = $serviceLoremIpsum->getSentences(mt_rand(2,5));
                $oMTOB->setDescription($description);
                $oMTOB->setManyToOneA($oMTOA);
                $aData[] = $oMTOB;
                if(mt_rand(1,100) %2 === 0){
                    $oMTOB2 = new ManyToOneB();
                    $description = $serviceLoremIpsum->getSentences(mt_rand(2,5));
                    $oMTOB2->setDescription($description);
                    $oMTOB2->setManyToOneA($oMTOA);
                    $aData[] = $oMTOB2;
                }
            } else {
                $aData[] = $oMTOA;
            }
        }
        return $aData;
    }

    /**
     * Return a list of specific values.
     *
     * @return array
     */
    public function getSpecificValues()
    {
        return [];
    }

    /**
     * Return the list of all values.
     *
     * @return array
     */
    public function getValues()
    {
        return array_merge($this->getSpecificValues(), $this->getAutomatedValues());
    }
}
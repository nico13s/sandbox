<?php

namespace Nico13s\CoreBundle\DataFixtures\Loader;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Exception\ValidatorException;

/**
 * Class AbstractLoader
 * @package Nico13s\CoreBundle\DataFixtures\Loader
 */
abstract class AbstractLoader extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Return a list of automated values.
     *
     * @return array
     */
    abstract public function getAutomatedValues();

    /**
     * Return a list of spefific values.
     *
     * @return array
     */
    abstract public function getSpecificValues();

    /**
     * Return the value to import in fixtures.
     *
     * @return array
     */
    abstract public function getValues();

    /**
     * Return the value of order to add fixtures
     *
     * @return array
     */
    abstract public function getOrder();

    /**
     * stub from ContainerAwareInterface
     *
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return object
     */
    public function getEntityManager(){
        return $this->getContainer()->get('doctrine.orm.default_entity_manager');
    }

    /**
     * Load set of datas.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager) {
        $aValue = $this->getValues();
        foreach ($aValue as $oValue) {
            $manager->persist($oValue);
        }
        $manager->flush();
    }

    /**
     * Validate an entity.
     *
     * @param $entity
     */
    protected function validateEntity($entity)
    {
        $validator = $this->container->get('validator');
        /** @var ConstraintViolationList $errors */
        $errors = $validator->validate($entity);

        if (0 < $errors->count()) {
            throw new ValidatorException((string) $errors);
        }
    }
}

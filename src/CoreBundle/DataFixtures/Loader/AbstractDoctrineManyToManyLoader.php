<?php

namespace Nico13s\CoreBundle\DataFixtures\Loader;

/**
 * Doctrine Many To One loader.
 *
 * Class AbstractDoctrineManyToManyLoader
 * @package Nico13s\CoreBundle\DataFixtures\Loader
 */
abstract class AbstractDoctrineManyToManyLoader extends AbstractLoader
{

    /**
     * Order of fixture execution.
     *
     * @return int
     */
    public function getOrder()
    {
        return 4;
    }
}

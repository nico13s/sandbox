<?php

namespace Nico13s\CoreBundle\DataFixtures\Loader;

/**
 * Doctrine One To One loader.
 *
 * Class AbstractDoctrineOneToOneLoader
 * @package Nico13s\CoreBundle\DataFixtures\Loader
 */
abstract class AbstractDoctrineOneToOneLoader extends AbstractLoader
{
    /**
     * Order of fixture execution.
     *
     * @return int
     */
    public function getOrder()
    {
        return 2;
    }
}

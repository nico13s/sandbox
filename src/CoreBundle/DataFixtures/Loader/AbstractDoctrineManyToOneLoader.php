<?php

namespace Nico13s\CoreBundle\DataFixtures\Loader;

/**
 * Doctrine Many To One loader.
 *
 * Class AbstractDoctrineManyToOneLoader
 * @package Nico13s\CoreBundle\DataFixtures\Loader
 */
abstract class AbstractDoctrineManyToOneLoader extends AbstractLoader
{

    /**
     * Order of fixture execution.
     *
     * @return int
     */
    public function getOrder()
    {
        return 3;
    }
}

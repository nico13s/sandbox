<?php

    namespace Nico13s\CoreBundle\DataFixtures\Loader;

    /**
     * Doctrine Simple loader.
     *
     * Class AbstractDoctrineSimpleLoader
     * @package Nico13s\CoreBundle\DataFixtures\Loader
     */
    abstract class AbstractDoctrineSimpleLoader extends AbstractLoader
    {
        /**
         * Order of fixture execution.
         *
         * @return int
         */
        public function getOrder()
        {
            return 1;
        }
    }

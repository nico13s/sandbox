# Symfony Command

Description
-----------

A command on Symfony is a powerful tool to launch script/tasks. You can find the documentation here: https://symfony.com/doc/current/console.html

By default, we can create a command based on Symfony\Component\Console\Command\Command but I prefer build command
based on Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand - you can more things - by example access to 
the container.

Simple example
--------------

In the file src/MainBundle/Command/DemoCommand.php - there is some features implemented:
* example of ContainerAwareCommand
* option and alias
* display text formatted in console

```
# Example to launch demo 
bin/console app:demo

# Example to launch demo with an option and high level of verbosity
bin/console app:demo -vvv --option=value
```

